#!usr/bin/python3
from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
import socket
import hashlib
import tkinter.messagebox as err

"""
Python 3.9
Author: Ernest Lassman
Latest Update: 22/05/2021
Users of this application must be registered by the owner of the application. They can not subscribe via this application.
The owner create login details for users to login with the login gui.
When logged in the user gets access to the main window with different user applications.
The goal is to be able to activate/run different applications from this main windows.
The user gets access to the applications he/she paid for only.
The applications exist on an offsite server.
"""


# TODO USE PySerial to connect to serial ports
# TODO USe PyODB as simple car diagnostic tool but first check out Delphi sourceCode(if possible)
# TODO reactivate secure login after design is done; by connecting database for storage of user details
#  and by hiding main window as long as user is not logged in. (login window is hidden behind main window for now)

# Login screen is made here
class Firstcreen:
    def __init__(self, master):
        self.master = master
        # self.stylingwin = ttk.Style()
        # self.newpasswin = Toplevel()
        master.title("Login")
        # self.stylingwin.theme_use("alt")

        # Screen measurements and place pop-up
        height = 400
        width = 600
        x = (master.winfo_screenwidth() // 2) - (width // 2)
        y = (master.winfo_screenheight() // 2) - (height // 2)

        master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        master.protocol('WM_DELETE_WINDOW', quit)
        master.rowconfigure(0, weight=1)
        master.rowconfigure(1, weight=1)
        master.rowconfigure(2, weight=1)
        master.rowconfigure(3, weight=1)

        master.columnconfigure(0, weight=1)
        master.columnconfigure(1, weight=1)

        # Title of the loginscreen
        header = Label(master, text="Scan System Login", font=("arial", 20, "bold"))
        header.grid(columnspan=2, rowspan=2)

        # Userdetails requested on the login screen
        usernamelbl = Label(master, text="Gebruikersnaam: ", font=("arial", 12, "bold"))
        usernamelbl.grid(row=1, sticky=SE)
        self.usernameent = Entry(master, font=('arial', 11))
        self.usernameent.grid(row=1, column=1, sticky=SW)

        wachtwoord = Label(master, text="Wachtwoord: ", font=("arial", 12, "bold"))
        wachtwoord.grid(row=2, sticky=NE)
        self.wachwoordent = Entry(master, show="*", font=("arial", 11))
        self.wachwoordent.grid(row=2, column=1, sticky=NW)

        # Loginscreen buttons
        labelbtns = LabelFrame(master, bd=0)
        labelbtns.grid(row=2, columnspan=2)

        loadfoto = Image.open("Application_Hub/maps-and-flags.png")
        self.foto = ImageTk.PhotoImage(loadfoto)

        # TODO send the input data to server for verification (function: activate)
        btnlogin = Button(labelbtns, text='Aanmelden', bd=4, width=100, height=30, image=self.foto, compound=LEFT,
                          font=("arial", 10, "bold"), command=activate)
        btnlogin.grid(row=0, column=0, sticky=EW)

        loadfoto = Image.open("Application_Hub/power-button.png")
        self.foto2 = ImageTk.PhotoImage(loadfoto)
        btnexit = Button(labelbtns, text='Afsluiten', image=self.foto2, height=30, compound=LEFT, bd=4,
                         font=("arial", 10, "bold"), command=self.exit_login)

        btnexit.grid(row=0, column=1, sticky=EW)

        loadfoto = Image.open("Application_Hub/password.png")
        self.foto3 = ImageTk.PhotoImage(loadfoto)
        self.resetbtn = Label(master, text='Wachtwoord opnieuw instellen', fg='blue', width=200, height=20,
                              image=self.foto3, compound=LEFT,
                              font=("arial", 9, "bold", "underline"))
        self.resetbtn.grid(row=3, column=0, columnspan=2, sticky=EW)
        self.resetbtn.bind("<Button-1>", self.resetpass)  # TODO after creating newpass window
        self.resetbtn.bind("<Enter>", self.grey_text)
        self.resetbtn.bind("<Leave>", self.blue_text)

        # Function to exit the login and close application

    def exit_login(self, event=None):
        self.master.quit()

        # Function pulls data from the inputboxes

    def getuserinpt(self):
        getusern = self.usernameent.get()
        getpassw = self.wachwoordent.get()
        total = [getusern, getpassw, ]
        return total

        # Function deletes data from inputboxes

    def removeuserinpt(self):
        self.usernameent.delete(0, 'end')
        self.wachwoordent.delete(0, 'end')

        # Styling of reset password button

    def blue_text(self, event=None):
        self.resetbtn.config(fg='blue', font=("arial", 9, "bold", "underline"))

    def grey_text(self, event=None):
        self.resetbtn.config(fg='black', font=("arial", 10, "bold", "underline"))

        # Shows reset password screen

    def resetpass(self, event=None):
        newpass.deiconify()


# The main application window is created
class NewScreen:
    def __init__(self, master):
        self.master = master
        self.master.title("Scan App")

        # Screen measurements and place pop-up
        height = 650
        width = 1100
        x = (master.winfo_screenwidth() // 2) - (width // 2)
        y = (master.winfo_screenheight() // 2) - (height // 2)
        master.geometry('{}x{}+{}+{}'.format(width, height, x, y))

        master.rowconfigure(0, weight=1)
        master.rowconfigure(1, weight=1)
        master.rowconfigure(2, weight=1)
        master.columnconfigure(0, weight=1)
        master.columnconfigure(1, weight=1)
        master.columnconfigure(2, weight=1)

        # main application window showing different apps
        verzameling = Frame(self.master, width=900, relief="groove", border=4)
        verzameling.grid(padx=100, row=3, pady=20)

        # header inside the main application window
        titleapp = Label(self.master, text="Gelieve een app te kiezen", font=("artial", 12, "bold"))
        titleapp.grid(row=0, sticky=SW, padx=120, pady=5)

        # discrition under the header inside the main application window
        titleapp = Label(self.master, text="Gelieve een app te kiezenf ezfds zefvs srfv sf f dsv sdfv dsf vsd vds "
                                           "ddsvfds fsv sddf sd ", font=("artial", 9, "italic"))
        titleapp.grid(row=1, sticky=NW, padx=120)

        # Buttons to activate the application of user choice
        firstapp = Button(verzameling, width=12, height=5, text="App1", font=("artial", 15, "bold"),
                          command=self.openodis)
        firstapp.grid(row=1, column=0, padx=50, pady=25)

        secondapp = Button(verzameling, width=12, height=5, text="App2", font=("artial", 15, "bold"))
        secondapp.grid(row=1, column=1, padx=50, pady=25)

        thirdapp = Button(verzameling, width=12, height=5, text="App3", font=("artial", 15, "bold"))
        thirdapp.grid(row=1, column=2, padx=50, pady=25)

        forthapp = Button(verzameling, width=20, height=7, state=DISABLED)
        forthapp.grid(row=2, column=0, padx=50, pady=20)

        fifthapp = Button(verzameling, width=20, height=7, state=DISABLED)
        fifthapp.grid(row=2, column=1, padx=50)

        sixthapp = Button(verzameling, width=20, height=7, state=DISABLED)
        sixthapp.grid(row=2, column=2, padx=50)

        seventhapp = Button(verzameling, width=20, height=7, state=DISABLED)
        seventhapp.grid(row=3, column=0, padx=50, pady=20)

        eighthapp = Button(verzameling, width=20, height=7, state=DISABLED)
        eighthapp.grid(row=3, column=1, padx=50)

        ninethapp = Button(verzameling, width=20, height=7, state=DISABLED)
        ninethapp.grid(row=3, column=2, padx=50)
        # master.withdraw()

        # opens up a new window with the chosen application functionalities

    def openodis(self):
        Odis.deiconify()
        app.withdraw()


class OdisAppOpen:
    def __init__(self, master):
        self.master = master
        # Screen measurements and place pop-up
        height = 650
        width = 1100
        x = (master.winfo_screenwidth() // 2) - (width // 2)
        y = (master.winfo_screenheight() // 2) - (height // 2)
        master.geometry('{}x{}+{}+{}'.format(width, height, x, y))

        master.rowconfigure(0, weight=1)
        master.rowconfigure(1, weight=1)
        master.rowconfigure(2, weight=1)
        master.columnconfigure(0, weight=1)
        master.columnconfigure(1, weight=1)
        master.columnconfigure(2, weight=1)
        master.withdraw()


# The window to reset password is created
class NewPassScreen:
    def __init__(self, master):
        self.master = master
        master.title("Nieuw wachtwoord")
        height = 200
        width = 600
        x = (master.winfo_screenwidth() // 2) - (width // 2)
        y = (master.winfo_screenheight() // 2) - (height // 2)
        master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        master.protocol('WM_DELETE_WINDOW', self.master.withdraw)
        master.rowconfigure(0, weight=1)
        master.rowconfigure(1, weight=1)
        master.rowconfigure(2, weight=1)
        master.rowconfigure(3, weight=1)

        master.columnconfigure(0, weight=1)
        master.columnconfigure(1, weight=1)
        master.columnconfigure(2, weight=1)
        master.columnconfigure(3, weight=1)

        labelcase = LabelFrame(self.master, width=80)
        labelcase.grid(row=1, column=1, columnspan=2, padx=20)
        ttk.Style().configure("TButton", relief="raised",
                              background="#ccc", font=("arial", 12, "bold"))

        emaillbl = Label(labelcase, text="e-mail: ", font=("arial", 13, "bold"))
        emaillbl.grid(row=1, column=0, sticky=W)
        emailentr = Entry(labelcase, font=("arial", 11), width=30)
        emailentr.grid(row=1, column=2, columnspan=2)
        emailsendpas = ttk.Button(labelcase, text="Verzenden", command=self.master.withdraw)
        emailsendpas.grid(row=1, column=4, sticky=EW)
        master.withdraw()


# Connection the client is connect to the main server and sending customer input.
class Client:
    def __init__(self):
        self.header2 = 150
        self.port = 3305
        self.server = "127.0.1.1"
        self.addr = (self.server, self.port)
        self.format = "utf-8"

        self.clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.clientsocket.connect(self.addr)

    # The user input is sent to the server where the listener lives.
    def sendusername(self, msg):
        for val in msg:
            message = val.encode(self.format)
            self.bmessage = (hashlib.sha512(message).hexdigest()).encode(self.format)
            msg_lenght = len(self.bmessage)
            string_msg_lenght = str(msg_lenght).encode(self.format)
            string_msg_lenght += b' ' * (self.header2 - len(string_msg_lenght))
            self.clientsocket.send(string_msg_lenght)
            self.clientsocket.send(self.bmessage)
        cltmsg = self.clientsocket.recv(1050)
        # When a response is received from the server, if response is positive, the user can login.
        # If response is not positive, error message is shown to user.
        if cltmsg:
            if cltmsg.decode(self.format) == self.bmessage.decode(self.format):
                app.deiconify()
                login.withdraw()
            elif cltmsg.decode(self.format) == "not found in database":
                login.option_add('*font', 'Arial -15')
                err.showerror("Fout", "Gegevens zijn niet correct.")
                window.removeuserinpt()
            elif cltmsg.decode(self.format) == "wrong password":
                login.option_add('*font', 'Arial -15')
                err.showerror("Fout", "Gegevens zijn niet correct.")
                window.removeuserinpt()


def activate():
    total = window.getuserinpt()
    cltsv = Client()
    cltsv.sendusername(total)


if __name__ == "__main__":
    app = Tk()
    login = Toplevel()
    newpass = Toplevel()
    Odis = Toplevel()

    window = Firstcreen(login)

    window2 = NewScreen(app)

    window3 = NewPassScreen(newpass)

    window4 = OdisAppOpen(Odis)

    login.mainloop()
