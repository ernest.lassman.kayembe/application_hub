#Application_Hub

Users of this application must be registered by the owner of the application. They can not subscribe via this application.
The owner create login details for users to login with the login gui.
When logged in the user gets access to the main window with different user applications.
The goal is to be able to activate/run different applications from this main windows.
The user gets access to the applications he/she paid for only.
The applications exist on an offsite server.
